#ライブラリのインポート
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt
from sklearn.datasets import fetch_20newsgroups
from sklearn.model_selection import train_test_split
from sklearn.svm import SVC 
from sklearn.linear_model import LogisticRegression
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.model_selection import cross_val_score
from sklearn.model_selection import KFold
import pandas as pd
import re
from nltk.corpus import stopwords
import lightgbm as lgbm
from sklearn.metrics import classification_report
from sklearn.metrics import confusion_matrix

#最初の数行を削除する関数
def delete_head(sentence):
    splits=sentence.split("\n")
    result=""
    for i in range(5,len(splits)):
        result+=" "+splits[i]
    return result

#文を単語のリストに変換する関数
def sentence2wordslist(sentence):
    #スペースでスプリット
    splits_list = sentence.split(" ")
    #もともと2つ以上のスペースがあった場所に対応する
    if "" in splits_list:
        splits_list.remove("")
    #有名なstop wordsの除去
    #リストに変換する際にやってしまう
    for word in stopwords.words('english'):
        if word in splits_list:
            splits_list.remove(word)
    return splits_list


#データの読み込み
all_data=fetch_20newsgroups(subset="all")


#データフレーム の作成
df_wrinting = pd.DataFrame(all_data["data"],columns=['data'])
df_target=pd.DataFrame(all_data["target"],columns=['target'])
df_all=pd.concat([df_wrinting, df_target], axis=1)
categories=all_data['target_names']

#前処理
#最初の数行を削除
df_all["data"]=df_all["data"].apply(lambda x : delete_head(x))
#全て小文字にする
df_all["data"]=df_all["data"].apply(lambda x : x.lower())
#数字を除去
df_all["data"]=df_all["data"].apply(lambda x : re.sub(r'[0-9０-９]+', " ", x))
#記号の除去
df_all["data"]=df_all["data"].apply(lambda x : re.sub(r'[\．_－―─！＠＃＄％＾＆\-‐|\\＊\“（）＿■×+α※÷⇒—●★☆〇◎◆▼◇△□(：〜～＋=＝)／*&^%$#@!~`){}［］…\[\]\"\'\”\’:;<>?＜＞〔〕〈〉？、。・,\./『』【】「」→←○《》≪≫\n\u3000]+', " ", x))

#tfidfを計算する
#次元が削減されるようにdfの閾値を適当に決めた
tfidf_vectorizer = TfidfVectorizer(analyzer=sentence2wordslist,min_df=50, max_df=0.5)
corpus = df_all["data"]
tfidfs = tfidf_vectorizer.fit_transform(corpus)
#計算結果よりデータフレーム を作成
tfidf_data = pd.DataFrame(tfidfs.toarray(),columns=["word"+str(i) for i in range(tfidfs.shape[1])])

#データの分割
#trainは訓練用、testは評価用
X_train, X_test, y_train, y_test = train_test_split(tfidf_data, df_all["target"], train_size=0.8, random_state=42)


# ロジスティック回帰実行
lr_model = LogisticRegression()
lr_model.fit(X_train, y_train) 
lr_y_pred = lr_model.predict(X_test)
print("ロジスティック回帰")
print(classification_report(lr_y_pred, y_test ,target_names=categories))
lr_cm = confusion_matrix(y_test, lr_y_pred)
print(sns.heatmap(lr_cm,xticklabels=categories, yticklabels=categories))


# svm実行
svm_model = SVC()               
svm_model.fit(X_train, y_train) 
svm_y_pred = svm_model.predict(X_test)
print("svm")
print(classification_report(svm_y_pred, y_test ,target_names=categories))
svm_cm = confusion_matrix(y_test, svm_y_pred)
print(sns.heatmap(svm_cm,xticklabels=categories, yticklabels=categories))


#lightgbm実行
lgbm_model = lgbm.LGBMClassifier()
lgbm_model.fit(X_train, y_train)
lgbm_y_pred = lgbm_model.predict(X_test)
print("lightgbm")
print(classification_report(lgbm_y_pred, y_test ,target_names=categories))
lgbm_cm = confusion_matrix(y_test, lgbm_y_pred)
print(sns.heatmap(lgbm_cm,xticklabels=categories, yticklabels=categories))


#交差検証
#時間があったら評価指標について考える
models = [LogisticRegression(),SVC(),lgbm.LGBMClassifier()]
for model in models:
    print(model)
    # 交差検証
    scores = cross_val_score(model, X_train, y_train)
    # 各分割におけるスコア
    print('Cross-Validation scores: {}'.format(scores))
    # スコアの平均値
    import numpy as np
    print('Average score: {}'.format(np.mean(scores)))